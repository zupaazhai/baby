<?php

/**
 * หน้าเริ่มต้นในกรณีที่เข้าสู่เว็บไซต์
 */
define('HOMEPAGE_VIEW', 'view/welcome/index');

/**
 * Base url
 */
define('BASE_URL', 'http://localhost');

/**
 * Enable/Disable debuging
 */
define('DEBUG', true);

/**
 * Database type
 */
define('DB_TYPE', 'mysql');

/**
 * Database server
 */
define('DB_SERVER', '127.0.0.1');

/**
 * Database name
 */
define('DB_NAME', '');

/**
 * Database username
 */
define('DB_USERNAME', '');

/**
 * Database password
 */
define('DB_PASSWORD', '');
