<?php

//run php migrate.php

include 'define.php';
include 'library/helper.php';

foreach (scandir('migrations') as $migrationFile) {
    $eachFileName = 'migrations/' . $migrationFile;
    if (is_file($eachFileName)) {
        include $eachFileName;
        $fnName = str_replace('.php', '', $migrationFile);
        call_user_func($fnName);
    }
}
