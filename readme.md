### Baby Sketch

ที่คุณกำลังใช้งานอยู่นี้ไม่ใช่ framework เป็นเพียงแค่ sketch โครงสร้างโปรเจ็คธรรมดาตัวหนึ่งที่มี concept ในการพัฒนาที่เรียกขึ้นมาเองว่า M (Module) - V (View) - A (Action) Concept เหมาะสำหรับผู้เริ่มต้นพัฒนาโปรเจ็คด้วย PHP ในระดับเริ่มต้น (Baby)

**M** odule - คือส่วนสำหรับเก็บฟังก์ชันหรือ class สำหรับ module นั้น เช่นใน module blog เก็บฟังก์ชันที่ใช้สำหรับการ get post, get comment

**V** iew - เก็บส่วนแสดงผลเช่นไฟล์ HTML

**A** ction - พฤติกรรมที่ต้องการทำเช่น save, update, delete เป็นต้น

---

Big thanks

Medoo database framework [https://medoo.in link](https://medoo.in)
Valitron: Easy Validation That Doesn't Suck [https://github.com/vlucas/valitron link](https://github.com/vlucas/valitron)
