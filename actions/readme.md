### Actions

action คือการกำหนด behavior ของสิ่งที่ต้องการทำการทำ route เพื่อมายัง action  สามารถทำได้โดยการเริ่มต้น route ด้วย `do` เช่น

`do\blog\post` baby blueprint จะทำการโหลดไฟล์ที่อยู่ภายใต้ directory `actions\blog\post.php` เป็นต้น
