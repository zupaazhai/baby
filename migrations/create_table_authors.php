<?php

function create_table_authors() {
    db()->query('CREATE TABLE IF NOT EXISTS authors(
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(100),
        lastname VARCHAR(100)
    )');
}
