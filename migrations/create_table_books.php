<?php

function create_table_books() {
    db()->query('CREATE TABLE IF NOT EXISTS books (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(100),
        author INT(6),
        year INT(4)
    )');
}
