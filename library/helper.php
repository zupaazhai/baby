<?php

session_start();

include 'library/vendor/Medoo.php';
include 'library/vendor/Validation.php';

/**
 * Init database instance
 *
 * @return Medoo
 */
function db() {
    $db = new Medoo(array(
        'database_type' => DB_TYPE,
        'database_name' => DB_NAME,
        'server' => DB_SERVER,
        'username' => DB_USERNAME,
        'password' => DB_PASSWORD
    ));

    return $db;
}

function validation($post, $fields = array(), $lang = null) {
    return new Validator($post, $fields, $lang, 'locale/validation');
}

/**
 * Include file
 *
 * @param string $path file path
 *
 * @return string
 */
function include_file($path) {
    if (file_exists($path)) {
        include $path;
    } else {
        include 'view/404.php';
    }
}

/**
 * Sanitize route
 *
 * @param string $routeStr route url string
 *
 * @return string
 */
function sanitize_route($routeStr) {
    $router = explode('/', $routeStr);

    $restrictDir = array('elements');

    if (empty($router)) {
        return false;
    }

    $_GET = array_merge($_GET, $router);

    $router = array_filter($router, function($route) {
        return !empty($route);
    });

    if (count($router) === 1 && file_exists('view/' . $router[0] . '/index.php')) {
        return 'view/' . $router[0] . '/index.php';
    }

    if (!empty($router[0]) && in_array($router[0], $restrictDir)) {
        return false;
    }

    if (count($router) >= 3 && strtolower($router[0]) == 'do') {
        return 'actions/' . $router[1] . '/' . $router[2] . '.php';
    }

    if (count($router) >= 2) {
        $router = array_slice($router, 0, 2);

        return 'view/' . implode('/', $router) . '.php';
    }

    return false;
}

/**
 * Control router
 *
 * @param array $get $get is $_GET
 *
 * @return void
 */
function router($get) {

    if (empty($get) || empty(sanitize_route($get['route']))) {
        return include HOMEPAGE_VIEW . '.php';
    }

    include_file(sanitize_route($get['route']));

    if (function_exists('before_action')) {
        before_action();
    }
}

/**
 * Get full link
 *
 * @param string $urlPath path
 *
 * @return string
 */
function l($urlPath) {
    return rtrim(BASE_URL, '/') . '/' . ltrim($urlPath, '/');
}

/**
 * Redirect
 *
 * @param string $urlPath url path
 *
 * @return void
 */
function redirect($urlPath) {
    header('Location: ' . l($urlPath));
    die();
}

/**
 * Load module
 *
 * @param string $moduleName module name
 *
 * @return void
 */
function load_module($moduleName) {
    return include 'modules/' . $moduleName . '.php';
}

/**
 * Load element
 *
 * @param string $elementName element name
 *
 * @return void
 */
function load_element($elementName) {
    $elementFile = 'view/elements/' . $elementName . '.php';

    if (!file_exists($elementFile)) {
        return;
    }

    return include $elementFile;
}

/**
 * Set flash message
 *
 * @param array $flashData flash data
 *
 * @return array
 */
function flash($flashData) {

    if (!empty($_SESSION['flash']) && is_array($_SESSION['flash'])) {
        return $_SESSION['flash'] = array_merge($_SESSION['flash'], $flashData);
    }

    return $_SESSION['flash'] = $flashData;
}

/**
 * Render flash message
 *
 * @return array
 */
function render_flash() {

    if (empty($_SESSION['flash'])) {
        return false;
    }

    $flashData = $_SESSION['flash'];
    unset($_SESSION['flash']);

    return $flashData;
}
