<?php
    header("HTTP/1.0 404 Not Found");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>404 - Something missing</title>
        <link href="https://fonts.googleapis.com/css?family=Maitree" rel="stylesheet">
        <style media="screen">
            body {
                font-family: 'Maitree', sans-serif;
                color: #d1cfcf;
            }
            h1 {
                font-size: 100px;
                margin: 0;
                padding: 0;
            }
            .container {
                width: 600px;
                margin: 0 auto;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>404</h1>
            <p>I'm not sure, how do you feel if someone missing from your life.</p>
        </div>
    </body>
</html>
