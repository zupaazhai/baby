<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Baby Sketch</title>
        <link href="https://fonts.googleapis.com/css?family=Maitree" rel="stylesheet">
        <style>
            body {
                font-family: 'Maitree', sans-serif;
            }
            .container {
                width: 600px;
                margin: 0 auto;
            }
            .text-center {
                text-align: center;
            }
            pre {
                padding: 1em;
                background-color: #f4f4f4;
                border: 1px solid #eee;
                border-radius: 2px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">Baby Sketch</h1>
            <p>
                ที่คุณกำลังใช้งานอยู่นี้ไม่ใช่ framework เป็นเพียงแค่ sketch โครงสร้างโปรเจ็คธรรมดาตัวหนึ่งที่มี concept ในการพัฒนาที่เรียกขึ้นมาเองว่า M (Module) - V (View) - A (Action) Concept เหมาะสำหรับผู้เริ่มต้นพัฒนาโปรเจ็คด้วย PHP ในระดับเริ่มต้น (Baby)
            </p>
            <p>
                <pre>
|____actions //  ไฟล์ js, css
| |____blog // action ย่อยภายใน
|____assets //  ไฟล์ js, css
|____index.php
|____library // library หรือ class อื่นๆ
|____modules // module
|____migrations // migrations file
| |____blog // module ย่อยภายใน
|____view
| |____welcome  // view ย่อย ภายใน
| | |____index.php //ไฟล์ index ภายใน view welcome
                </pre>
            </p>
            <p>
                <strong>M</strong> odule - คือส่วนสำหรับเก็บฟังก์ชันหรือ class สำหรับ module นั้น เช่นใน module blog เก็บฟังก์ชันที่ใช้สำหรับการ save และ get blog
            </p>
            <p>
                <strong>V</strong> iew - เก็บส่วนแสดงผลเช่นไฟล์ HTML
            </p>
            <p>
                <strong>A</strong> ction - พฤติกรรมที่ต้องการทำเช่น save, update, delete เป็นต้น
            </p>
        </div>
    </body>
</html>
